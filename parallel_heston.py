from math import sqrt
from time import perf_counter as pc

import numpy as np

from pricer import Pricer, Pricer2

# ============================================================================

def heston(rand, reps, periods, params):
    ''' returns N trajectories from an Heston model'''
    v0 = params.get("v0")
    rho = params.get("rho")
    dt = params.get("dt")
    mu = params.get("mu")
    k = params.get("k")
    theta = params.get("theta")
    eta = params.get("eta")
    s0 = params.get("s0")

    v = np.full(shape=reps, fill_value=v0)
    X = np.empty(shape=(periods+1, reps))
    G = rand.normal(size=(2, periods, reps))
    np.multiply(G[1], sqrt(1-rho*rho), out=G[1])
    X[0] = 0
    for i in range(periods):
        g = G[0][i]
        w = G[1][i]
        vdt = np.sqrt(v*dt)
        X[i+1] = X[i] + (mu - v/2)*dt + vdt*(rho*g + w)
        v += k*(theta - v)*dt + eta*vdt*g
        np.maximum(v, .0, out=v)
    return s0*np.exp(X)


def pricing_rule(trajs, params):
    strike = params.get("strike")
    return np.where(trajs > strike, trajs - strike, .0)

# ============================================================================

class HestonMartingale(Pricer2):

    def _generate_underlying(self, rand) -> np.ndarray:
        """heston trajs"""
        periods = self.params.get("times")
        reps = self.params.get("reps")
        v0 = self.params.get("v0")
        rho = self.params.get("rho")
        dt = self.params.get("dt")
        mu = self.params.get("mu")
        k = self.params.get("k")
        theta = self.params.get("theta")
        eta = self.params.get("eta")
        s0 = self.params.get("s0")

        v = np.full(shape=reps, fill_value=v0)
        X = np.empty(shape=(periods+1, reps))
        G = rand.normal(size=(2, periods, reps))
        np.multiply(G[1], sqrt(1-rho*rho), out=G[1])
        X[0] = 0
        for i in range(periods):
            g = G[0][i]
            w = G[1][i]
            vdt = np.sqrt(v*dt)
            X[i+1] = X[i] + (mu - v/2)*dt + vdt*(rho*g + w)
            v += k*(theta - v)*dt + eta*vdt*g
            np.maximum(v, .0, out=v)
        return s0*np.exp(X)

    def _pricing_rule(self, trajs: np.ndarray) -> np.ndarray:
        """vanilla call"""
        strike = self.params.get("strike")
        return np.where(trajs > strike, trajs - strike, .0)


# ============================================================================

def main():

    params = {
              "dt":         1/12, 
              "s0":         4, 
              "mu":         .0, 
              "sd":         .3, 
              "v0":         .024, 
              "theta":      .024, 
              "k":          .2, 
              "eta":        .4, 
              "rho":        -.5,
              "strike":     3.7
              }

    tot = int(input("log_2 of reps? "))
    
    heston_vanilla_call = Pricer(params, tot, 12, 4, heston, pricing_rule)
    s = pc()
    es, err = heston_vanilla_call.simulate()
    print("took %4.4f seconds 1"%(pc()-s))
    for a, b in zip(es, err):
        print("%4.4f +- %4.4f"%(a, b))


    heston_vanilla_call_2 = HestonMartingale(params, tot, 12, 4)
    s2 = pc()
    es, err = heston_vanilla_call_2.simulate()
    print("took %4.4f seconds 2"%(pc()-s2))
    for a, b in zip(es, err):
        print("%4.4f +- %4.4f"%(a, b))
    

if __name__=="__main__":
    main()
    # input()
