from abc import ABC, abstractmethod

from concurrent.futures import ProcessPoolExecutor as ProcessPool
from copy import deepcopy
from typing import Tuple, Callable

import numpy as np


class Pricer:
    """
    # Pricer
    class used to do MonteCarlo simulation for pricing derivatives

    ### @params
    - a dict that contains all the params of the simulation
    - this dict is then unpacked into the custom functions

    ### @tot_reps
    (int) log base 2 of the total number of simulations

    ### @times
    (int) how many time intervals to simulate

    ### @workers
    - (int) number of simultaneous processes
    - can be only one of (1, 2, 4, 8, 16, 32)

    ## attention:
    - the following functions must return 2D numpy ndarrays
    - where array[0] is the starting point for all the generated trajectories

    ### @generator_func
    - function that generates the underlyng process
    - first argument is the random instance
    - second is how many trajectories are produced
    - third is how many time steps are simulated
    - fourth is the previuos 'params' parameter, that has to be unpacked as shown
    #### example:

    >>> def example(rand, reps, times, params):
    ...     times = params.get("sigma")
    ...     return rand.normal(size=(times, reps))*sigma

    ### @pricing_func
    this is the pricing rule. unpacks argument as in the above example, but has no rand instance
    
    instead it has a first argument that is the result of the generator_func
    #### example

    >>> def example(trajs, params):
    ...     strike = params.get("strike")
    ...     return np.maximum(trajs, strike)

    """

    def __init__(self, params:dict, tot_reps:int, times:int, workers:int, generator_func:Callable, pricing_func:Callable):
        # reserved keywords
        assert isinstance(params, dict)
        assert isinstance(tot_reps, int)
        assert isinstance(times, int)
        assert isinstance(generator_func, Callable)
        assert isinstance(pricing_func, Callable)
        for k in params.keys():
            assert isinstance(k, str)
            if k in ("tot_reps", "mc_reps", "reps", "workers", "times"):
                raise KeyError(f"Please don't use key={k} in the 'params' dict!")
        self.params = deepcopy(params)
        _tot_reps = 1<<tot_reps # 2**n
        assert workers in (1, 2, 4, 8, 16, 32) # max = 2**5
        self.generator_func = generator_func
        self.pricing_func = pricing_func
        # compute internal params
        mc_reps = _tot_reps // workers # reps that every MC process will do -> is automatically 2**p
        reps = 1<<10 # number of trajs for every "generator_func" call # 2**10
        if mc_reps < reps:
            raise ValueError(f"reps={mc_reps} < p_reps={reps} -> raise 'tot_reps'!")
        self.params.update({"tot_reps":_tot_reps,
                            "mc_reps":mc_reps, 
                            "reps":reps,
                            "times":times,
                            "workers":workers
                            })
        
    def _pricing_rule(self, trajs:np.ndarray) -> np.ndarray:
        """custom pricing rule"""
        return self.pricing_func(trajs, self.params)

    def _generate_underlying(self, rand, reps, times) -> np.ndarray:
        """this generates the underlying trajectories"""
        j = self.generator_func(rand, reps, times, self.params) 
        # ensure correct matrix type and orientation
        # assert isinstance(j, np.ndarray)
        # assert all(j[0]==j[0][0])
        return j

    def _mc(self, rand) -> Tuple[np.ndarray, np.ndarray]:
        """MonteCarlo routine"""
        times = self.params.get("times")
        mc_reps = self.params.get("mc_reps") # tot_reps // workers
        reps = self.params.get("reps") # 1<<10
        # math
        es = np.zeros(shape=(times+1), dtype=np.double)
        err = np.zeros(shape=(times+1), dtype=np.double)
        for _ in range(mc_reps // reps):
            trajs = self._generate_underlying(rand, reps, times)
            trajs = self._pricing_rule(trajs)
            es += trajs.sum(axis=1)
            err += (trajs*trajs).sum(axis=1)
        return (es, err)

    def simulate(self) -> Tuple[np.ndarray, np.ndarray]: 
        """execute the mc function in parallel processes"""
        times = self.params.get("times")
        workers = self.params.get("workers")
        tot_reps = self.params.get("tot_reps")
        # multiprocessing
        with ProcessPool(max_workers=workers) as pool:
            # fix rng seed here
            rng = tuple((np.random.RandomState(seed) for seed in range(workers)))
            res = pool.map(self._mc, rng)
        # aggregate stats
        ES = np.zeros(shape=(times+1), dtype=np.double)
        ERR = np.zeros(shape=(times+1), dtype=np.double)
        for es, err in res:
            ES += es
            ERR += err
        ES /= tot_reps
        ERR = 3*np.sqrt((ERR/tot_reps - ES*ES)/tot_reps)
        return ES, ERR

# ============================================================================

"""maybe easier to use, but slower"""


class Pricer2(ABC):
    """
    # Pricer
    class used to do MonteCarlo simulation for pricing derivatives

    ### @params
    - a dict that contains all the params of the simulation
    - this dict is then unpacked into the custom functions

    ### @tot_reps
    (int) log base 2 of the total number of simulations

    ### @times
    (int) how many time intervals to simulate

    ### @workers
    - (int) number of simultaneous processes
    - can be only one of (1, 2, 4, 8, 16, 32)

    ## attention:
    - one needs to inherit from this class and override the 2 functions
    - the following functions must return 2D numpy ndarrays
    - where array[0] is the starting point for all the generated trajectories

    ### @_generate_underlying (abstractmethod)
    - function that generates the underlyng process
    - first argument is the random instance
    - second is how many trajectories are produced
    - third is how many time steps are simulated
    #### example:

    >>> def _generate_underlying(self, rand, reps, times):
    ...     times = self.params.get("sigma")
    ...     return rand.normal(size=(times, reps))*sigma

    ### @_pricing_rule (abstractmethod)
    this is the pricing rule. unpacks argument as in the above example, but has no rand instance
    
    instead it has a first argument that is the result of the generator_func
    #### example

    >>> def _pricing_rule(self, trajs):
    ...     strike = self.params.get("strike")
    ...     return np.maximum(trajs, strike)

    """

    def __init__(self, params:dict, tot_reps:int, times:int, workers:int):
        # reserved keywords
        assert isinstance(params, dict)
        assert isinstance(tot_reps, int)
        assert isinstance(times, int)
        for k in params.keys():
            assert isinstance(k, str)
            if k in set(("tot_reps", "mc_reps", "reps", "workers", "times")):
                raise KeyError(f"Please don't use key={k} in the 'params' dict!")
        self.params = deepcopy(params)
        _tot_reps = 1<<tot_reps # 2**n
        assert workers in (1, 2, 4, 8, 16, 32) # max = 2**5
        # compute internal params
        mc_reps = _tot_reps // workers # reps that every MC process will do -> is automatically 2**p
        reps = 1<<10 # number of trajs for every "generator_func" call # 2**10
        if mc_reps < reps:
            raise ValueError(f"reps={mc_reps} < p_reps={reps} -> raise 'tot_reps'!")
        self.params.update({"tot_reps":_tot_reps,
                            "mc_reps":mc_reps, 
                            "reps":reps, # 1<<10
                            "workers":workers,
                            "times":times})
        
    @abstractmethod
    def _generate_underlying(self, rand) -> np.ndarray:
        pass

    @abstractmethod
    def _pricing_rule(self, trajs:np.ndarray) -> np.ndarray:
        pass
        
    def _mc(self, rand) -> Tuple[np.ndarray, np.ndarray]:
        """MonteCarlo routine"""
        times = self.params.get("times")
        mc_reps = self.params.get("mc_reps") # tot_reps // workers
        reps = self.params.get("reps") # 1<<10
        # math
        es = np.zeros(shape=(times+1), dtype=np.double)
        err = np.zeros(shape=(times+1), dtype=np.double)
        for _ in range(mc_reps // reps):
            trajs = self._generate_underlying(rand)
            trajs = self._pricing_rule(trajs)
            es += trajs.sum(axis=1)
            err += (trajs*trajs).sum(axis=1)
        return (es, err)

    def simulate(self) -> Tuple[np.ndarray, np.ndarray]: 
        """execute the mc function in parallel processes"""
        times = self.params.get("times")
        workers = self.params.get("workers")
        tot_reps = self.params.get("tot_reps")
        # multiprocessing
        with ProcessPool(max_workers=workers) as pool:
            # fix rng seed here
            rng = tuple((np.random.RandomState(seed) for seed in range(workers)))
            res = pool.map(self._mc, rng)
        # aggregate stats
        ES = np.zeros(shape=(times+1), dtype=np.double)
        ERR = np.zeros(shape=(times+1), dtype=np.double)
        for es, err in res:
            ES += es
            ERR += err
        ES /= tot_reps
        ERR = 3*np.sqrt((ERR/tot_reps - ES*ES)/tot_reps)
        return ES, ERR
